//
//  main.cpp
//  preparing
//
//  Created by Gordiy Rushynets on 3/4/19.
//  Copyright © 2019 Gordiy Rushynets. All rights reserved.
//

#include <iostream>
#include <string>
#include <fstream>

using namespace std;

class Gun
{
public:
    virtual void shoot()
    {
        cout << "BANG!!!" << endl;
    }
};

class SubmachineGun : public Gun
{
public:
    // override must check even if shoot's signature are alike to Gun::shoot()
    void shoot() override
    {
        cout << "BANG BANG BANG !!!" << endl;
    }
};

class Bazooka : public Gun
{
public:
    void shoot() override
    {
        cout << "BUM !!!" << endl;
    }
};

class Player
{
public:
    void shoot(Gun *gun)
    {
        gun->shoot();
    }
};


int main(int argc, const char * argv[]) {
    /*
     Поліморфізм дозволяє, використовуючи вказівник на базовий клас (в даному випадку Gun), змінювати
     поведінку функцій наслідуваних класів(в даному випадку SubmachineGun). ОБОВ'ЯЗКОВИМИ є ключові слова:
     virtual і override.
     */
    
    Gun gun;
    SubmachineGun machinegun;
    Bazooka bazooka;
    
    // Вказівник weapon може містити в собі як пістолет, так і пістолет-кулемет(mechinegun),
    // оскільки клас SubmachineGun наслідується від класу Gun
    
    Gun *weapon = &machinegun;
    
    // викличиться метод shoot() з класу SubmachineGun, тому що вказівник *weapon зберігає ссилку на нього
    weapon->shoot();
    
    
    // функція shoot() з класу Player отримує як аргумент вказівник на базовий клас Gun. Коли ми її викликаємо
    // ми можемо передати ссилку на об’єкт класу в якому міститься дана функція
    Player player;
    // викличе функцію shoot() з класу Gun
    player.shoot(&gun);
    
    // викличе функцію shoot() з класу SubmachineGun
    player.shoot(&machinegun);
    
    // викличе функцію shoot() з класу Bazooka
    player.shoot(&bazooka);
    
    return 0;
}
